package net.dasturlash.fragmentframework.fragments;

import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import net.dasturlash.fragmentframework.MainActivity;
import net.dasturlash.fragmentframework.R;

/**
 * Created by Sherzodbek on 14.10.2017. (FragmentFramework)
 */

public class ContentFragment extends BaseFragment {
    private Button button1;
    private Button button2;

    public ContentFragment() {
        super(R.layout.fragment_content);
    }

    @Override
    public void onInitView(View view) {
        button1 = view.findViewById(R.id.button1);
        button2 = view.findViewById(R.id.button2);
    }

    @Override
    public void onInitDefaultValue() {
        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getContext(), "Salom", Toast.LENGTH_SHORT).show();
            }
        });
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity)getActivity()).goNext(new InfoFragment());
            }
        });
    }
}
