package net.dasturlash.fragmentframework.fragments;

import android.view.View;

import net.dasturlash.fragmentframework.R;

/**
 * Created by Sherzodbek on 14.10.2017. (FragmentFramework)
 */

public class InfoFragment extends BaseFragment {
    public InfoFragment() {
        super(R.layout.fragment_info);
    }

    @Override
    public void onInitView(View view) {

    }

    @Override
    public void onInitDefaultValue() {

    }
}
